package com.music.entity;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data //提供getter、setter方法
@NoArgsConstructor //无参构造
public class Result {
    private boolean success;
    private Integer code;
    private String message;
    private Object data;

    //不需要返回数据时使用
    public Result(ResultCode code) {
        this.success = code.success;
        this.code = code.code;
        this.message = code.message;
    }

    public Result(ResultCode code,String message) {
        this.success = code.success;
        this.message = message;
        this.code = code.code;
    }

    public Result(ResultCode code,Object data) {
        this.success = code.success;
        this.code = code.code;
        this.message = code.message;
        this.data = data;
    }

    public Result(Integer code,String message,boolean success) {
        this.code = code;
        this.message = message;
        this.success = success;
    }

    /*
     * 调用ResultCode类封装常用的返回数据
     */
    public static Result SUCCESS(){
        return new Result(ResultCode.SUCCESS);
    }

    public static Result ERROR(){
        return new Result(ResultCode.SERVER_ERROR);
    }

    public static Result FAIL(){
        return new Result(ResultCode.FAIL);
    }
}
