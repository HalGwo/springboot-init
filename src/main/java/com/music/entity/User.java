package com.music.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Microlink
 */
@Data
@NoArgsConstructor
@ApiModel("用户表User")
public class User {
    @ApiModelProperty(value = "用户名", required = true)
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("性别 0:男 1:女")
    private int sex;
    @ApiModelProperty("头像url")
    private String avatar;
    @ApiModelProperty("城市")
    private String city;
    @ApiModelProperty("省份")
    private String province;

}